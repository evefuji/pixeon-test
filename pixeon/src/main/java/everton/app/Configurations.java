package everton.app;

import java.util.ResourceBundle;

public class Configurations {

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("test");
	public static String SERVER_URL = BUNDLE.getString("server.url");
	public static String SERVICES_PREFFIX = BUNDLE.getString("services.preffix");
	
	public static String SERVER_URL_TO_TEST = SERVER_URL + SERVICES_PREFFIX;

	public static String CLINICA_URL = SERVER_URL_TO_TEST + "clinica";
	public static String VISUALIZADOR_LOCATOR_URL = SERVER_URL_TO_TEST + "visualizadorlocator";
	public static String EXAME_URL = SERVER_URL_TO_TEST + "exame"; 
}
