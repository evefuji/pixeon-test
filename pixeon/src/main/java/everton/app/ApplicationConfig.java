/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package everton.app;

import java.util.Set;

/**
 *
 * @author everton
 */
@javax.ws.rs.ApplicationPath("rest")
public class ApplicationConfig extends javax.ws.rs.core.Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<Class<?>>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(everton.service.ClinicaFacadeREST.class);
        resources.add(everton.service.ExameFacadeREST.class);
        resources.add(everton.service.VisualizadorLocatorFacadeREST.class);
        resources.add(everton.service.example.VisualizadorExampleREST.class);
    }
    
}
