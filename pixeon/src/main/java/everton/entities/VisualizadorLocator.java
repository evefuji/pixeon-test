/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package everton.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author everton
 */
@Entity
@Table(name = "visualizador_locator")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VisualizadorLocator.findAll", query = "SELECT v FROM VisualizadorLocator v"),
    @NamedQuery(name = "VisualizadorLocator.findById", query = "SELECT v FROM VisualizadorLocator v WHERE v.id = :id"),
    @NamedQuery(name = "VisualizadorLocator.findByUrl", query = "SELECT v FROM VisualizadorLocator v WHERE v.url = :url")})
public class VisualizadorLocator implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "url")
    private String url;

    public VisualizadorLocator() {
    }

    public VisualizadorLocator(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlElement(required=true, nillable=true)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VisualizadorLocator)) {
            return false;
        }
        VisualizadorLocator other = (VisualizadorLocator) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "everton.entities.VisualizadorLocator[ id=" + id + " ]";
    }
    
}
