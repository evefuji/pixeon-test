package everton.service;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import everton.entities.Exame;
import everton.service.example.UrlContainerDTO;

@javax.ejb.Stateless
public class ExameService  extends AbstractService<Exame>{

    @PersistenceContext(unitName = "PixeonPU")
    private EntityManager em;

    public ExameService() {
        super(Exame.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public String[] getUrlImages(Exame exame){
    	
    	Client client = ClientBuilder.newClient();
    	return client.target(
    			exame.getClinica().getVisualizadorLocator().getUrl()+
    			exame.getId()
    		).request(MediaType.APPLICATION_JSON)
    		.get(UrlContainerDTO.class).getUrls();
    	
    }
    
}
