package everton.service.example;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UrlContainerDTO {

	private String[] urls;

	public UrlContainerDTO() {
	}
	
	public UrlContainerDTO(String[] urls) {
		this.urls = urls;
	}

	@XmlElement(name="url")
	public String[] getUrls() {
		return urls;
	}

	public void setUrls(String[] urls) {
		this.urls = urls;
	}
}
