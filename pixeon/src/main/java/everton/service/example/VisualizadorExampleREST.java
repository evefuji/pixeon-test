package everton.service.example;


@javax.ws.rs.Path("visualizadorexample")
public class VisualizadorExampleREST {
	
	@javax.ws.rs.GET
    @javax.ws.rs.Path("{id}")
    @javax.ws.rs.Produces({"application/xml", "application/json"})
	public UrlContainerDTO list(@javax.ws.rs.PathParam("id") String id){
		return new UrlContainerDTO (
				new String[]{
						"http://google.com.br/?q="+id,
						"http://google.com.br/?q="+id
				}
		);
	}

}
