package everton.service;

import everton.entities.VisualizadorLocator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author everton
 */
@javax.ejb.Stateless
@javax.ws.rs.Path("visualizadorlocator")
public class VisualizadorLocatorFacadeREST extends AbstractService<VisualizadorLocator> {
    @PersistenceContext(unitName = "PixeonPU")
    private EntityManager em;

    public VisualizadorLocatorFacadeREST() {
        super(VisualizadorLocator.class);
    }

    @javax.ws.rs.POST
    @Override
    @javax.ws.rs.Consumes({"application/xml", "application/json"})
    public VisualizadorLocator create(VisualizadorLocator entity) {
        return super.create(entity);
    }

    @javax.ws.rs.PUT
    @javax.ws.rs.Path("{id}")
    @javax.ws.rs.Consumes({"application/xml", "application/json"})
    public VisualizadorLocator edit(@javax.ws.rs.PathParam("id") Integer id, VisualizadorLocator entity) {
        return super.edit(entity);
    }

    @javax.ws.rs.DELETE
    @javax.ws.rs.Path("{id}")
    public void remove(@javax.ws.rs.PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @javax.ws.rs.GET
    @javax.ws.rs.Path("{id}")
    @javax.ws.rs.Produces({"application/xml", "application/json"})
    public VisualizadorLocator find(@javax.ws.rs.PathParam("id") Integer id) {
        return super.find(id);
    }

    @javax.ws.rs.GET
    @Override
    @javax.ws.rs.Produces({"application/xml", "application/json"})
    public List<VisualizadorLocator> findAll() {
        return super.findAll();
    }

    @javax.ws.rs.GET
    @javax.ws.rs.Path("{from}/{to}")
    @javax.ws.rs.Produces({"application/xml", "application/json"})
    public List<VisualizadorLocator> findRange(@javax.ws.rs.PathParam("from") Integer from, @javax.ws.rs.PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @javax.ws.rs.GET
    @javax.ws.rs.Path("count")
    @javax.ws.rs.Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
