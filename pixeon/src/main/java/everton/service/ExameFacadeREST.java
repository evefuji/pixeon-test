package everton.service;

import java.util.List;

import javax.inject.Inject;

import everton.entities.Exame;

/**
 *
 * @author everton
 */
@javax.ws.rs.Path("exame")
public class ExameFacadeREST {
	
	@Inject
	private ExameService service;
	
    @javax.ws.rs.POST
    @javax.ws.rs.Consumes({"application/xml", "application/json"})
    public Exame create(Exame entity) {
        entity = service.create(entity);
        entity.setUrlImages(service.getUrlImages(entity));
		return entity;
    }

    @javax.ws.rs.PUT
    @javax.ws.rs.Path("{id}")
    @javax.ws.rs.Consumes({"application/xml", "application/json"})
    public Exame edit(@javax.ws.rs.PathParam("id") Integer id, Exame entity) {
        entity = service.edit(entity);
        entity.setUrlImages(service.getUrlImages(entity));
		return entity;
    }

    @javax.ws.rs.DELETE
    @javax.ws.rs.Path("{id}")
    public void remove(@javax.ws.rs.PathParam("id") Integer id) {
    	service.remove(service.find(id));
    }

    @javax.ws.rs.GET
    @javax.ws.rs.Path("{id}")
    @javax.ws.rs.Produces({"application/xml", "application/json"})
    public Exame find(@javax.ws.rs.PathParam("id") Integer id) {
        Exame entity = service.find(id);
        entity.setUrlImages(service.getUrlImages(entity));
		return entity;
    }

    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/xml", "application/json"})
    public List<Exame> findAll() {
        List<Exame> exames = service.findAll();
        exames.forEach(exame -> exame.setUrlImages(service.getUrlImages(exame)));
		return exames;
    }

    @javax.ws.rs.GET
    @javax.ws.rs.Path("{from}/{to}")
    @javax.ws.rs.Produces({"application/xml", "application/json"})
    public List<Exame> findRange(@javax.ws.rs.PathParam("from") Integer from, @javax.ws.rs.PathParam("to") Integer to) {
        List<Exame> exames = service.findRange(new int[]{from, to});
        exames.forEach(exame -> exame.setUrlImages(service.getUrlImages(exame)));
		return exames;
    }

    @javax.ws.rs.GET
    @javax.ws.rs.Path("count")
    @javax.ws.rs.Produces("text/plain")
    public String countREST() {
        return String.valueOf(service.count());
    }

}
