package everton.service;

import java.util.Date;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import everton.app.Configurations;
import everton.entities.Clinica;
import everton.entities.Exame;
import everton.entities.VisualizadorLocator;

public class ExameTest {

	private static VisualizadorLocator locator;
	private static Clinica clinica;

	private static Client client;
	
	@BeforeClass
	public static void before(){
		
		locator = new VisualizadorLocator();
		locator.setUrl(Configurations.SERVER_URL_TO_TEST+"visualizadorexample/");
		
		client = ClientBuilder.newClient();
		locator = client.target(Configurations.VISUALIZADOR_LOCATOR_URL)
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.json(locator), VisualizadorLocator.class);

		clinica = new Clinica();
		clinica.setNome("Teste");
		clinica.setVisualizadorLocator(locator);

		clinica = client.target(Configurations.CLINICA_URL)
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.json(clinica), Clinica.class);
	}
	
	@Test
	public void test() {
		Exame exame = new Exame();
		exame.setDataHora(new Date(System.currentTimeMillis() + 100000000));
		exame.setClinica(clinica);
		exame = client.target(Configurations.EXAME_URL)
			.request(MediaType.APPLICATION_JSON)
			.post(Entity.json(exame), Exame.class);

		client.target(Configurations.EXAME_URL+"/"+exame.getId())
			.request().delete();
		
		Assert.assertArrayEquals(new String[]{"http://google.com.br/?q="+exame.getId(), "http://google.com.br/?q="+exame.getId()}, exame.getUrlImages());
		
	}

	@AfterClass
	public static void after(){
		
		client.target(Configurations.CLINICA_URL+"/"+clinica.getId())
				.request().delete();
		
		client.target(Configurations.VISUALIZADOR_LOCATOR_URL+"/"+locator.getId())
				.request().delete();
	}
}
